ARG GO_VERSION=1.17.5
FROM golang:${GO_VERSION}-alpine AS build

RUN apk add --no-cache git
WORKDIR /src
COPY ./go.mod ./go.sum ./
RUN go mod download
COPY ./ ./

ARG VERSION
ARG SHA
ARG DATE
RUN go get github.com/Masterminds/sprig/v3
RUN go get gitlab.com/davidthamwf/smtp-dove/internal/backend
RUN go get gitlab.com/davidthamwf/smtp-dove/internal/config
RUN go get gitlab.com/davidthamwf/smtp-dove/internal/session

RUN CGO_ENABLED=0 go build \
  -ldflags="\
  -X 'main.version=${VERSION}' \
  -X 'main.commit=${SHA}' \
  -X 'main.date=${DATE}' \
  -X 'main.builtBy=github'" \
  -o /smtp-dove ./cmd/smtp-dove
RUN /smtp-dove --version

# Final container
# FROM gcr.io/distroless/static AS final
FROM alpine:latest AS final

# Set environment parameters and variables
ENV USER_ID=appuser1
ENV AUTH_TOKEN=mytokenvalue1234
ENV HTTP_POST_URL=https://my.endpoint.com/mail
ARG UID=1000
ARG GID=1000

ARG VERSION
ARG SHA
ARG DATE

LABEL org.opencontainers.image.created=${DATE}
LABEL org.opencontainers.image.version=${VERSION}
LABEL org.opencontainers.image.revision=${SHA}
LABEL org.opencontainers.image.url="https://gitlab.com/davidthamwf/smtp-dove.git"
LABEL org.opencontainers.image.vendor="David Tham"
LABEL org.opencontainers.image.author="David Tham"
LABEL org.opencontainers.image.title="smtp-dove"
LABEL org.opencontainers.image.description="SMTP to HTTP POST with Go text template"

RUN addgroup -g "${GID}" nonroot
RUN adduser --disabled-password --ingroup nonroot --no-create-home --uid "${UID}" nonroot
USER nonroot:nonroot
COPY --from=build --chown=nonroot:nonroot /smtp-dove /smtp-dove
COPY --from=build --chown=nonroot:nonroot /src/template.txt /template.txt
CMD /smtp-dove --url $HTTP_POST_URL --header "Authorization: Bearer $AUTH_TOKEN" --host 0.0.0.0 --port 1025 --template """$(cat /template.txt)"""